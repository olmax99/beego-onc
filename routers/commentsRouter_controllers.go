package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context/param"
)

func init() {
	beego.GlobalControllerRouter["beego-onc/controllers:UserController"] = append(beego.GlobalControllerRouter["beegi-onc/controllers:UserController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/create",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})
}
